﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Packing.Common
{
    public interface IPacker
    {
        void Pack(Package package, IEnumerable<Rectangle> objects);
    }
}
