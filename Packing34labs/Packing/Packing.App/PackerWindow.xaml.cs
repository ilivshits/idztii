﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Packing.Common;
using Rectangle = Packing.Common.Rectangle;
using RectControl = System.Windows.Shapes.Rectangle;

namespace Packing.App
{
    /// <summary>
    /// Interaction logic for PackerWindow.xaml
    /// </summary>
    public partial class PackerWindow : Window
    {
        private Package _package;
        private Random _rnd = new Random();

        public PackerWindow(Package package)
        {
            InitializeComponent();

            _package = package;

            Width = package.Width + 150;
            Height = package.Height + 150;

            Canvas.Width = package.Width;
            Canvas.Height = package.Height;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < _package.Levels.Count; i++)
            {
                Level level = _package.Levels[i];

                for (int j = 0; j < level.FloorObjects.Count; j++)
                {
                    Rectangle rect = level.FloorObjects[j];

                    var rectControl = GetRectPrototype(rect);

                    Canvas.SetLeft(rectControl, level.FloorObjects.Take(j).Sum(r => r.Width));
                    Canvas.SetBottom(rectControl, _package.Levels.Take(i).Sum(l => l.Height));

                    Canvas.Children.Add(rectControl);
                }

                for (int j = 0; j < level.CeilingObjects.Count; j++)
                {
                    Rectangle rect = level.CeilingObjects[j];

                    var rectControl = GetRectPrototype(rect);

                    Canvas.SetRight(rectControl, level.CeilingObjects.Take(j).Sum(r => r.Width));
                    Canvas.SetBottom(rectControl, _package.Levels.Take(i).Sum(l => l.Height) + level.Height - rect.Height);

                    Canvas.Children.Add(rectControl);
                }
            }
        }

        private RectControl GetRectPrototype(Rectangle rect)
        {
            var color = new SolidColorBrush(Color.FromRgb((byte)_rnd.Next(256), (byte)_rnd.Next(256), (byte)_rnd.Next(256)));
            var rectControl = new RectControl()
            {
                Stroke = color,
                StrokeThickness = 2,
                Fill = color,
                Width = rect.Width,
                Height = rect.Height
            };

            return rectControl;
        }
    }
}