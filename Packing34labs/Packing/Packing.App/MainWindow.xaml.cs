﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Packing.BFDH;
using Packing.Common;
using Packing.FloorCeiling;
using Rectangle = Packing.Common.Rectangle;

namespace Packing.App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            using (var reader = new StreamReader("Rectangles.json"))
            {
                rectsJsonTb.Text = reader.ReadToEnd();
            }
        }

        private void packButton_Click(object sender, RoutedEventArgs e)
        {
            string packageWidthString = packageWidthTb.Text;
            int packageWidth;
            if (!int.TryParse(packageWidthString, out packageWidth))
            {
                MessageBox.Show($"Package width should be number.");
                return;
            }

            string rectJson = rectsJsonTb.Text;
            var rects = JsonConvert.DeserializeObject<List<Rectangle>>(rectJson);

            IPacker packer = null;

            switch (((ComboBoxItem)methodCb.SelectedItem).Tag.ToString())
            {
                case "BFDH":
                    packer = new BfdhPacker();
                    break;
                case "FloorCeiling":
                    packer = new FloorCeilingPacker();
                    break;
            }

            if (packer == null)
            {
                MessageBox.Show($"Internal error, unresolve method type.");
                return;
            }

            var package = new Package(packageWidth);
            packer.Pack(package, rects);

            var packerWindow = new PackerWindow(package);
            packerWindow.Show();
        }

        private void generateRectsBtn_Click(object sender, RoutedEventArgs e)
        {
            string numToGenerateString = numToGenerateRectsTb.Text;
            int numToGenerate;
            if (!int.TryParse(numToGenerateString, out numToGenerate))
            {
                MessageBox.Show($"Count of generated Rects should be number.");
                return;
            }

            var rnd = new Random();

            var rects = new List<Rectangle>();
            for (int i = 0; i < numToGenerate; i++)
            {
                rects.Add(new Rectangle(rnd.Next(3, 150), rnd.Next(3, 150)));
            }

            rectsJsonTb.Text = JsonConvert.SerializeObject(rects);
        }
    }
}