﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;
using Packing.Common;

namespace Packing.FloorCeiling
{
    public class FloorCeilingPacker : IPacker
    {
        public void Pack(Package package, IEnumerable<Rectangle> objects)
        {
            var sortedObjects = objects.OrderByDescending(r => r.Height).ToList();

            for (var i = 0; i < sortedObjects.Count; i++)
            {
                var rect = sortedObjects[i];
                AddToSuitableLevel(package, rect);
            }
        }

        private void AddToSuitableLevel(Package package, Rectangle rect)
        {
            if (!package.Levels.Any())
            {
                var newLevel = new Level();
                newLevel.FloorObjects.Add(rect);
                package.Levels.Add(newLevel);

                return;
            }

            foreach (var level in package.Levels)
            {
                if (CanGoToFloor(package, level, rect))
                {
                    level.FloorObjects.Add(rect);
                    return;
                }

                if (CanGoToCeiling(package, level, rect))
                {
                    level.CeilingObjects.Add(rect);
                    return;
                }
            }

            var newLvl = new Level();
            newLvl.FloorObjects.Add(rect);
            package.Levels.Add(newLvl);

        }

        private bool CanGoToFloor(Package package, Level level, Rectangle rect)
        {
            int xPoint = level.FloorWidth + rect.Width;

            if (xPoint > package.Width)
            {
                return false;
            }

            int? ceilingRectHeight = GetCeilingRectHeight(level, package.Width - xPoint);
            return !ceilingRectHeight.HasValue || ceilingRectHeight.Value + rect.Height <= level.Height;
        }

        private bool CanGoToCeiling(Package package, Level level, Rectangle rect)
        {
            int xPoint = level.CeilingWidth + rect.Width;

            if (xPoint > package.Width)
            {
                return false;
            }

            int? floorRectHeight = GetFloorRectHeight(level, package.Width - xPoint);
            return !floorRectHeight.HasValue || floorRectHeight.Value + rect.Height <= level.Height;
        }

        private int? GetFloorRectHeight(Level level, int xPoint)
        {
            if (level.FloorWidth < xPoint)
            {
                return null;
            }

            int curPoint = 0;
            foreach (var rect in level.FloorObjects)
            {
                curPoint += rect.Width;
                if (curPoint >= xPoint)
                {
                    return rect.Height;
                }
            }

            return null;
        }

        private int? GetCeilingRectHeight(Level level, int xPoint)
        {
            if (level.CeilingWidth < xPoint)
            {
                return null;
            }

            int curPoint = 0;
            foreach (var rect in level.CeilingObjects)
            {
                curPoint += rect.Width;
                if (curPoint >= xPoint)
                {
                    return rect.Height;
                }
            }

            return null;
        }
    }
}
