﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyGraphControls
{
    /// <summary>
    /// Interaction logic for NodeControl.xaml
    /// </summary>
    public partial class NodeControl : UserControl
    {
        public Node Node
        {
            get;
            set;
        }

        public NodeControl(Node node)
        {
            InitializeComponent();
            this.Node = node;

        }

        private void NodeControl_Loaded(object sender, RoutedEventArgs e)
        {
            TextID.Content = Node.ID.ToString();
        }

        public void SetCoordinates(int x, int y)
        {
            this.Margin = new Thickness(x, y, 0, 0);

        }
    }
}
