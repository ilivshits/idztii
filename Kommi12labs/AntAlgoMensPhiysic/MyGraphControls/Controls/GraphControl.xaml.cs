﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyGraphControls
{
    /// <summary>
    /// Interaction logic for GraphControl.xaml
    /// </summary>
    public partial class GraphControl : UserControl
    {
        private int[][] matrix;
        private int SIZE;
        private List<Node> nodes;
        private List<Path> paths;
        private List<NodeControl> nodeControls;
        private List<PathControl> pathControls;

        public GraphControl(int[][] matrix)
        {
            if (matrix.GetLength(0) != matrix[0].GetLength(0))
                throw new Exception("Матрица не квадратная");
            InitializeComponent();
            this.matrix = matrix;
            this.SIZE = matrix.GetLength(0);
            nodes = new List<Node>();
            paths = new List<Path>();
            nodeControls = new List<NodeControl>();
            pathControls = new List<PathControl>();
            InitGraph();
        }

        public void InitGraph()
        {
            Random rnd = new Random();
            Node.ResetID();


            double deltaAngle = Math.PI * (360 / SIZE) / 180.0;
            for (int i = 0; i < SIZE; i++)
            {
                nodes.Add(new Node());
                nodeControls.Add(new NodeControl(nodes.Last()));
                double angle = deltaAngle * i;
                int x = (int)(300 + 300 * Math.Cos(angle));
                int y = (int)(300 + 300 * Math.Sin(angle));
                nodeControls.Last().SetCoordinates(x, y);
                cnv.Children.Add(nodeControls.Last());
            }
            for (int i = 0; i < SIZE; i++)
            {

                for (int j = 0; j < SIZE; j++)
                {

                    if (i != j && matrix[i][j] > 0)
                    {
                        paths.Add(new Path(nodes[i], nodes[j], matrix[i][j]));
                        pathControls.Add(new PathControl(paths.Last()));
                        pathControls.Last().SetCoordinates(
                            nodeControls[i].Margin.Left,
                            nodeControls[i].Margin.Top,
                            nodeControls[j].Margin.Left,
                            nodeControls[j].Margin.Top
                            );
                        cnv.Children.Add(pathControls.Last());
                    }
                }
            }

        }


    }
}
