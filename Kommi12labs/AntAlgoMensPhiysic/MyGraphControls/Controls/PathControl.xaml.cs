﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyGraphControls
{
    /// <summary>
    /// Interaction logic for PathControl.xaml
    /// </summary>
    public partial class PathControl : UserControl
    {
        public PathControl(Path path)
        {
            InitializeComponent();
            this.Path = path;
        }

        private void PathControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.Label.Text = this.Path.Length.ToString();
        }

        public void SetCoordinates(double x1, double y1, double x2, double y2)
        {
            Point beg = new Point(x1 + 15, y1 + 15);
            Point fin = new Point(x2 + 15, y2 + 15);
            Point tmp;

            double angle1 = Math.Atan2(x1 - x2, y1 - y2) - Math.PI / 2;

            tmp = CrossPoint(fin, beg, 15);
            x1 = this.MainLine.X1 = tmp.X + 5 * Math.Sin(angle1);
            y1 = this.MainLine.Y1 = tmp.Y + 5 * Math.Cos(angle1);
            tmp = CrossPoint(beg, fin, 15);
            x2 = this.MainLine.X2 = tmp.X + 5 * Math.Sin(angle1);
            y2 = this.MainLine.Y2 = tmp.Y + 5 * Math.Cos(angle1);

            double angle = Math.Atan2(x1 - x2, y1 - y2);

            this.ArrowLineLeft.X1 = x2;
            this.ArrowLineLeft.Y1 = y2;
            this.ArrowLineLeft.X2 = x2 + 15 * Math.Sin(0.3 + angle);
            this.ArrowLineLeft.Y2 = y2 + 15 * Math.Cos(0.3 + angle);

            this.ArrowLineRight.X1 = x2;
            this.ArrowLineRight.Y1 = y2;
            this.ArrowLineRight.X2 = x2 + 15 * Math.Sin(angle - 0.3);
            this.ArrowLineRight.Y2 = y2 + 15 * Math.Cos(angle - 0.3);

            this.Label.Margin = new Thickness(
                (this.MainLine.X1 + this.MainLine.X2) / 2,
                (this.MainLine.Y1 + this.MainLine.Y2) / 2,
                0,
                0
                );
        }

        public Point CrossPoint(Point sPoint, Point center, double r)
        {
            double signX, signY, deltaX, deltaY;
            signX = Math.Sign(center.X - sPoint.X);
            signY = Math.Sign(center.Y - sPoint.Y);
            double k = Math.Abs((center.Y - sPoint.Y) / (center.X - sPoint.X));
            deltaX = r * Math.Cos(Math.Atan(k));
            deltaY = r * Math.Sin(Math.Atan(k));

            return new Point(center.X - signX * deltaX, center.Y - signY * deltaY);
        }

        public Path Path
        {
            get;
            set;
        }
    }
}
