﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGraphControls
{
    public class Path
    {
        public Path(Node from, Node to, Int32 len)
        {
            this.NodeFrom = from;
            this.NodeTo = to;
            this.Length = len;
        }

        #region Свойства

        public Node NodeFrom
        {
            get;
            set;
        }

        public Node NodeTo
        {
            get;
            set;
        }

        public Int32 Length
        {
            get;
            set;
        }

        #endregion
    }
}
