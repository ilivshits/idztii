﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGraphControls
{
    public class Node
    {
        private int id;

        private static int tmpID = 0;

        public static void ResetID()
        {
            tmpID = 0;
        }

        public Node()
        {
            this.id = tmpID++;
        }

        public int ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
    }
}
