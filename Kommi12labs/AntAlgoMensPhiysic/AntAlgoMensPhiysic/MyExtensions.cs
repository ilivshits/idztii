﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace AntAlgoMensPhysique
{
    public static class MyExtensions
    {
        public static void SetCoordinates(this Thickness th, int x, int y)
        {
            th = new Thickness(x, y, 0, 0);
        }
    }
}
