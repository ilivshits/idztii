﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyGraphControls;
using AntMethod;

namespace AntAlgoMensPhysique
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Controls
        GraphControl gControl;
        #endregion

        #region Properties
        private Int32 graphControlIndex = -1;
        #endregion

        #region Constants
        Int32 NOWAY = 999999;
        #endregion

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {



        }

        private void runBtn_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();

            int matrixSize = rnd.Next(3, 7);
            if (matrixSize % 2 == 0) matrixSize++;
            if (graphControlIndex != -1)
                MyGrid.Children.RemoveAt(graphControlIndex);

            int[][] mtr;
            mtr = GenerateMatrix(matrixSize);

            /*mtr[0] = new int[6] { 0, 5, NOWAY, NOWAY, 8, 1 };
            mtr[1] = new int[6] { 7, 0, 1, NOWAY, 5, NOWAY };
            mtr[2] = new int[6] { NOWAY, 3, 0, 6, 1, NOWAY };
            mtr[3] = new int[6] { NOWAY, NOWAY, 8, 0, 6, 7 };
            mtr[4] = new int[6] { 7, 7, 3, 8, 0, 7 };
            mtr[5] = new int[6] { 7, NOWAY, NOWAY, 5, NOWAY, 0 };*/

            resultLbl.Content = "Best Circuit is: " + AntColonyProgram.StartProgram(mtr);

            for (int i = 0; i < matrixSize; i++)
                for (int j = 0; j < matrixSize; j++)
                {
                    if (mtr[i][j] == NOWAY)
                        mtr[i][j] = 0;
                    if (i == j)
                        mtr[i][j] = 0;
                }

            gControl = new GraphControl(mtr);
            graphControlIndex = MyGrid.Children.Add(gControl);
        }

        private Int32[][] GenerateMatrix(int matrixSize)
        {
            Int32[][] mtr;
            Random rnd = new Random();
            mtr = new Int32[matrixSize][];

            for (int i = 0; i < matrixSize; i++)
            {
                mtr[i] = new Int32[matrixSize];
                for (int j = 0; j < matrixSize; j++)
                {
                    if (i == j)
                        mtr[i][j] = NOWAY;
                    else
                        mtr[i][j] = rnd.Next(1, 5);
                }
            }
            return mtr;
        }
    }
}
