﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionMin.App
{
    static class StringExtensions
    {
        public static List<int> AllIndexesOf(this string source, char ch)
        {
            var foundIndexes = new List<int>();

            for (int i = source.IndexOf(ch); i > -1; i = source.IndexOf(ch, i + 1))
            {
                // for loop end when i=-1 ('a' not found)
                foundIndexes.Add(i);
            }

            return foundIndexes;
        }
    }
}
