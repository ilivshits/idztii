﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionMin.App
{
    public class SwarmIntelligence
    {
        public Swarm Find(int numbOfSwarm, int countOfSwarm, int iter, double locAcc, double globAcc, Func<IList<double>, double> func)
        {
            Random rand = new Random((int)DateTime.Now.Ticks);

            Swarm[] sw = new Swarm[numbOfSwarm];
            for (int i = 0; i < numbOfSwarm; i++)
                sw[i] = new Swarm(countOfSwarm, func);


            for (int j = 0; j < numbOfSwarm; j++)
            {

                //если текущее положение лучше
                if (func(sw[j].xCoord) < func(sw[j].lSBest))
                {
                    for (int i = 0; i < sw[j].xCoord.Length; i++)
                        sw[j].lSBest[i] = sw[j].xCoord[i];
                }
                //если текущее лучшее положение лучше глобального
                if (func(sw[j].lSBest) < func(Swarm.GSBest))
                {
                    for (int i = 0; i < sw[j].xCoord.Length; i++)
                        Swarm.GSBest[i] = sw[j].lSBest[i];
                }
            }

            for (int k = 0; k < iter; k++)
            {

                for (int j = 0; j < numbOfSwarm; j++)
                {
                    for (int i = 0; i < sw[0].speed.Length; i++)
                    {
                        sw[j].speed[i] += rand.NextDouble() * locAcc * (sw[j].lSBest[i] - sw[j].xCoord[i]) + rand.NextDouble() * globAcc * (Swarm.GSBest[i] - sw[j].xCoord[i]);
                        sw[j].xCoord[i] += sw[j].speed[i];
                    }

                    //обновляем значения функции
                    sw[j].y = func(sw[j].xCoord);
                }


                for (int j = 0; j < numbOfSwarm; j++)
                {

                    //если текущее положение лучше
                    if (func(sw[j].xCoord) < func(sw[j].lSBest))
                    {
                        for (int i = 0; i < sw[j].xCoord.Length; i++)
                            sw[j].lSBest[i] = sw[j].xCoord[i];
                    }
                    //если текущее лучшее положение лучше глобального
                    if (func(sw[j].lSBest) < func(Swarm.GSBest))
                    {
                        for (int i = 0; i < sw[j].xCoord.Length; i++)
                            Swarm.GSBest[i] = sw[j].lSBest[i];
                    }
                }

            }

            Array.Sort(sw);
            return sw[0];
        }
        public class Swarm : IComparable
        {
            public double[] lSBest;//лучшая найденная частицей точка
            public static double[] GSBest;//лучшая точка из пройденных всеми частицами системы
            public double[] xCoord;//текущая позиция частицы
            public double[] speed;//скорость частицы
            public double y;

            public int CompareTo(object obj)
            {//сортировка
                if (obj == null) return 1;
                var geneElite = obj as Swarm;
                if (geneElite == null)
                    throw new Exception("Unable to compare");

                return y.CompareTo(geneElite.y);
            }

            public Swarm(int Count, Func<IList<double>, double> func)
            {
                xCoord = new double[Count];
                speed = new double[Count];
                lSBest = new double[Count];
                GSBest = new double[Count];

                Random rand = new Random((int)DateTime.Now.Ticks);

                for (int i = 0; i < Count; i++)
                {
                    xCoord[i] = rand.NextDouble();
                    speed[i] = rand.NextDouble();
                    lSBest[i] = rand.NextDouble();
                    if (GSBest == null)
                        GSBest[i] = rand.NextDouble();

                }
                y = func(xCoord);
            }
        }
    }
}
