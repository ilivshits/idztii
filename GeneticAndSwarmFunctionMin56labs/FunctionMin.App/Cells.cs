﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FunctionMin.App
{
    class Cells
    {
        public double minYinCells
        {
            get { return minY; }
        }

        private Func<IList<double>, double> _func;

        public static double mutation;
        public static double crossover;
        private Gene[] gen;
        private static int Count;
        private double minY = 0, maxY = 0;

        public Cells(int numbOfCells, int CountOfCells, Func<IList<double>, double> func)
        {
            _func = func;

            gen = new Gene[numbOfCells];
            Count = CountOfCells;
            for (int i = 0; i < numbOfCells; i++)
                gen[i] = new Gene(Count, func);
        }
        public void Crossover()
        {
            Gene[] genChild = new Gene[gen.Length * 2];//генов в два раза больше теперь
            Random temp = new Random();
            for (int i = 0; i < genChild.Length; i++)
            {
                genChild[i] = new Gene();
                if (i < genChild.Length / 2 - 1)
                    genChild[i] = gen[i];//половина будет из родителей
                else
                {
                    genChild[i].xCoord = new double[Count];

                    for (int j = 0; j < Count; j++)
                        genChild[i].xCoord[j] = gen[temp.Next((int)(this.gen.Length * crossover), this.gen.Length)].xCoord[temp.Next(Count)];//вторая половина плучит случайные координаты от случайных родителей

                    genChild[i].y = _func(genChild[i].xCoord);
                }

            }
            this.gen = genChild;

        }
        public void Mutation()
        {
            Random temp = new Random();
            double rand;
            int rnd;
            FindMinMax();//определяем максимум и минимум
            for (int i = 0; i < gen.Length; i++)
            {
                rnd = temp.Next(100);
                rand = (gen[i].y - minY) / (maxY - minY) * 100;//функция для определения приспособленности
                if (rand > rnd)
                    gen[i].xCoord[temp.Next(Count)] *= (double)(temp.Next(1000, (int)(mutation * 1000))) / 1000;
            }
        }
        public void Selection()
        {
            Gene[] genHalf = new Gene[gen.Length / 2];//выживет только половина
            Array.Sort(gen);//сортируем массив по приспособленности
            var genHalfLength = gen.Length;
            for (int i = gen.Length / 2, j = 0; i < genHalfLength; i++, j++)
                genHalf[j] = new Gene(gen[i]);

            gen = genHalf;
        }
        public double AverageY()
        {//среднее значение y
            double s = 0;
            for (int i = 0; i < gen.Length; i++)
                s += gen[i].y;
            return s / gen.Length;

        }
        public void FindMinMax()
        {//поиск минимума и максимума
            minY = gen[0].y;
            maxY = minY;
            for (int i = 0; i < gen.Length; i++)
            {
                if (minY > gen[i].y)
                    minY = gen[i].y;
                if (maxY < gen[i].y)
                    maxY = gen[i].y;
            }
        }
        public string AverageX()
        {//средние значения координат
            double[] x = new double[Count];
            string result = "";
            for (int i = 0; i < gen.Length; i++)
                for (int j = 0; j < x.Length; j++)
                    x[j] += gen[i].xCoord[j];

            for (int i = 0; i < x.Length; i++)
            {
                x[i] /= gen.Length;
                result += x[i].ToString("F5") + "; ";
            }

            return result;


        }
        public string EliteGeneX()
        {//самый приспособленный ген
            string result = "";
            for (int i = 0; i < Count; i++)
                result += gen[gen.Length - 1].xCoord[i].ToString("F5") + "; ";
            return result;
        }
        private class Gene : IComparable
        {
            public double[] xCoord;
            public double y;

            public int CompareTo(object obj)
            {//сортировка
                if (obj == null) return 1;
                var geneElite = obj as Gene;
                if (geneElite == null)
                    throw new Exception("Unable to compare");

                return -y.CompareTo(geneElite.y);
            }

            public Gene()
            {
            }

            public static double Generate()
            {//гсч
                RNGCryptoServiceProvider c = new RNGCryptoServiceProvider();
                byte[] randomNumber = new byte[8];
                double result;
                c.GetBytes(randomNumber);
                result = (double)(BitConverter.ToInt64(randomNumber, 0)) / (long.MaxValue / 10);
                return result;
            }

            public Gene(Gene gene)
            {
                xCoord = gene.xCoord;
                y = gene.y;
            }

            public Gene(int Count, Func<IList<double>, double> func)
            {
                xCoord = new double[Count];
                for (int i = 0; i < Count; i++)
                    xCoord[i] = Generate();
                y = func(xCoord);
            }


        }
    }
}
