﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FunctionMin.App
{
    public partial class Form1 : Form
    {
        int n = 0;
        List<double> population;

        private readonly Func<IList<double>, double> _func = list => Math.Pow(list[0], 2) + Math.Pow(list[1], 2)/*100 * Math.Pow(Math.Pow(list[0], 2) - list[1], 2) + Math.Pow(1 - list[0], 2)*/;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton6.Checked == true)
                GenAlg();
            else
                SwIn();
        }

        private void GenAlg()
        {
            double eps = Convert.ToDouble(textBox2.Text);//погрешность
            int numberX = 2;//число координат
            Cells.mutation = Convert.ToDouble(textBox1.Text);//коэффициент мутации

            n = 0;//число циклов
            //проверка числа переменных

            //кто сможет размножатся
            if (radioButton1.Checked)
                Cells.crossover = 0;
            else if (radioButton2.Checked)
                Cells.crossover = 0.5;
            else
                Cells.crossover = 0.1;


            Cells a = new Cells((int)numericUpDown2.Value, numberX, _func);
            population = new List<double>();
            richTextBox1.Font = new Font(RichTextBox.DefaultFont, FontStyle.Regular);
            richTextBox1.Text = "Начальная популяция: ";
            richTextBox1.Text += "\r\nСреднее значение функции:" + a.AverageY().ToString("F5") + "\r\n Средние значения координат x: " + a.AverageX() + "\r\n";
            do
            {
                population.Add(a.AverageY());
                a.Crossover();//размножение
                a.Mutation();//мутация
                a.Selection();//селекция
                n++;
            }
            while (Math.Abs(population[n - 1] - a.AverageY()) > eps && n < numericUpDown1.Value);

            richTextBox1.Text += "Популяция спустя " + n + " поколений:\r\n";
            richTextBox1.Text += "Среднее значение функции:" + a.AverageY().ToString("F5") + "\r\n Средние значения координат x: " + a.AverageX() + "\r\n";
            richTextBox1.Text += "Самый приспособленный ген:\r\n";
            richTextBox1.Text += "значение функции:" + a.minYinCells.ToString("F5") + "\r\n значения координат x: " + a.EliteGeneX() + "\r\n";

            richTextBox1.Select(0, 22);
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            richTextBox1.Select(richTextBox1.Text.IndexOf(richTextBox1.Lines[3]), 30);
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            richTextBox1.Select(richTextBox1.Text.IndexOf(richTextBox1.Lines[6]), 27);
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
        }
        private void SwIn()
        {
            int numberX = 2;//число координат
            int numNumb = (int)numericUpDown2.Value;//количество точек
            double locA = Convert.ToDouble(textBox4.Text);
            double globA = Convert.ToDouble(textBox1.Text);
            int iter = Convert.ToInt32(textBox2.Text);

            SwarmIntelligence sw = new SwarmIntelligence();
            SwarmIntelligence.Swarm swarm = sw.Find(numNumb, numberX, iter, locA, globA, _func);

            richTextBox1.Text = $"Координаты: ({swarm.xCoord[0]}, {swarm.xCoord[1]})";
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            textBox2.Text = "0,01";//погрешность
            numericUpDown1.Value = 500;//максимальное количество итераций
            numericUpDown2.Value = 500;//количество клеток
            textBox4.Visible = false;//для роя частиц
            textBox1.Text = "1,250";//коэффициент мутации
            radioButton1.Checked = true;//размножение
            radioButton6.Checked = true;//выбор алгоритма
            textBox4.Text = "0,5";//локальное ускорение
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            var selectFromListRadio = sender as RadioButton;
            if (selectFromListRadio.Checked)
            {
                label2.Text = "Макс.итер.:";
                textBox2.Text = "100";
                textBox4.Visible = true;
                label3.Text = "Локальное ускорение:";
                label5.Text = "Глоб. ускор.:";
                label6.Visible = false;
                groupBox1.Visible = false;
                textBox1.Text = "0,7";
            }
            else
            {
                label2.Text = "Погрешность:";
                textBox2.Text = "0,01";
                label2.Visible = true;
                textBox4.Visible = false;
                label3.Text = "Макс. кол-во поколений:";
                label5.Text = "Мутировать в";
                label6.Visible = true;
                groupBox1.Visible = true;
                textBox1.Text = "1,250";
            }
        }
    }
}
